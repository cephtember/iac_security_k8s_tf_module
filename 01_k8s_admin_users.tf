resource "kubernetes_service_account" "administrators" {
  for_each    = var.admin_users 
  metadata {
    name      = each.value.name
    namespace = each.value.namespace
  }

  depends_on = [ kubernetes_namespace.namespaces ]
}

resource "kubernetes_secret" "administrators" {
  for_each    = var.admin_users 
  metadata {
    name = "${each.value.name}-secret"
    namespace = "${each.value.namespace}"
    annotations = {
      "kubernetes.io/service-account.name" = "${each.value.name}"
    }
  }
  type = "kubernetes.io/service-account-token"

  depends_on = [kubernetes_service_account.administrators]
}

resource "kubernetes_cluster_role_binding" "administrators" {
  for_each    = var.admin_users 
  metadata {
    name = "${each.value.name}"
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = "cluster-admin"
  }
  subject {
    kind      = "ServiceAccount"
    name      = "${each.value.name}"
    namespace = "${each.value.namespace}"
  }

  depends_on = [kubernetes_service_account.administrators]
}