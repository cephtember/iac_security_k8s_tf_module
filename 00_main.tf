provider "helm" {
  kubernetes {
    config_path = var.kubeconfig_file
  }
}

provider "kubernetes" {
  config_path = var.kubeconfig_file
}

provider "kubectl" {
  config_path = var.kubeconfig_file
}

provider "vault" {
  
}

terraform {
  required_providers {
    template = {
      source  = "hashicorp/template"
      version = "2.2.0"
    }

    kubectl = {
      source  = "gavinbunney/kubectl"
      version = "1.14.0"
    }

    helm = {
      source  = "hashicorp/helm"
      version = "2.9.0"
    }

    vault = {
      source = "hashicorp/vault"
      version = "3.23.0"
    }
  }
}