# Модуль terraform компонентов безопасности k8s

Модуль используется в качестве личных экспериментов и сделан просто ради фана.

> На самом деле я просто устал пересобирать
> себе кластер для тестирования каких либо
> сценариев / чартов / приложений.

## TODO
- Установка Vault в качестве центра сертификации
- Установка Vault в качестве транзитного сервера шифрования
- Добавление пользователей Vault 
- Установка Certmanager и подвязка его к Vault
- Установка PAM Teleport и тестовое создание пользователей
- Установка Sonarqube

## Features

- Добавление пользователей и неймспейсов
- Добавление администраторов
-

## Installation

 Приводим terraform main.tf фаил к следующему виду

  ```module "security_k8s_modules" {
#  Путь до модуля (может быть как git repo или путь до папки)
#  
  source = "git@gitlab.com:cephtember/iac_security_k8s_tf_module.git"

#  Путь до kubeconfig файла
#  
  kubeconfig_file = "k8s_local_kube_config"

#  Список namespaces
#  
  namespaces = {
    devops = {
        description = "Namespace администраторов"
        name = "devops"
    }
    test_department = {
        description = "Namespace для тестов"
        name = "test-department"
    },
  }

#  Список пользователей кластер администраторов
#
  admin_users = {
    vladimir_filatov = {
        description = "Основной администратор"
        name = "filatovv"
        namespace = "devops"
    }
  }
#  Список пользователей разработчиков
#
  developer_users = {
    test_user = {
        description = "Пользователь"
        name = "test-user"
        namespace = "test-department"
    }
  }
}

#
#

  ```

## Plugins

| Plugin | README |
| ------ | ------ |
| 00_Main Module | [https://gitlab.com/cephtember/iac_tf_project] |
| 01_CloudInit Module | [https://gitlab.com/cephtember/iac_cloudinit_tf_module] |
| 02_Core Module | [https://gitlab.com/cephtember/iac_core_k8s_tf_module] |
| 03_Security Module | [https://gitlab.com/cephtember/iac_security_k8s_tf_module] |
