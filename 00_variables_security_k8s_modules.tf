variable "kubeconfig_file" {
  type    = string
  default = "~/.kube/config"
}

variable "dns_zone" {
  type = string
  default = "k8s.loc"
}

variable "lb_master_node_ip" {
  type = string
  default = "10.23.31.70"
}

variable "pki_intermediate_cert_request" {
  type = object({
    beta = object({
      enable = bool
    })
    master = object({
      enable = bool
    })
  })
  default = {
    beta = {
      enable = true
    }
    master = {
      enable = false
    }
  }
}

variable "pki_signed_cert" {
  type = object({
    beta = object({
      enable = bool
    })
    master = object({
      enable = bool
    })
  })
  default = {
    beta = {
      enable = true
    }
    master = {
      enable = false
    }
  }
}


variable "namespaces" {
  type = map(any)
  default = {
    test_department = {
      description = "Namespace для тестов"
      name = "test-department"
    },
    devops = {
        description = "Namespace администраторов"
        name = "devops"
    }
  }
}

variable "developer_users" {
  type = map(any)
  default = {
    test_user = {
      name = "test-user"
      namespace = "test-department"
      description = "Пользователь для тестов"
    }
  }
}

variable "admin_users" {
  type = map(any)
  default = {
    vladimir_filatov = {
      name = "filatovv"
      namespace = "devops"
      description = "Основной админ пользователь"
    }
  }
}