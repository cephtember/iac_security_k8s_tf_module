resource "kubernetes_namespace" "cert-manager" {
  metadata {
    name = "cert-manager"
  }
}


resource "kubernetes_service_account" "issuer" {
  metadata {
    name      = "issuer"
    namespace = kubernetes_namespace.cert-manager.metadata[0].name
  }

  depends_on = [
    kubernetes_namespace.cert-manager,
  ]
}


resource "kubernetes_secret" "issuer" {
  metadata {
    name      = "issuer-token-lmzpj"
    namespace = kubernetes_namespace.cert-manager.metadata[0].name
    annotations = {
      "kubernetes.io/service-account.name" = "issuer"
    }
  }
  type = "kubernetes.io/service-account-token"

  depends_on = [
    kubernetes_service_account.issuer,
  ]
}


resource "kubectl_manifest" "cert-manager" {
  yaml_body = <<YAML
apiVersion: cert-manager.io/v1
kind: Issuer
metadata:
  name: vault-issuer
  namespace: cert-manager
spec:
  vault:
    server: http://vault.vault:8200
    path: pki/sign/cluster-dot-local
    auth:
      kubernetes:
        mountPath: /v1/auth/kubernetes
        role: issuer
        secretRef:
          name: issuer-token-lmzpj
          key: token
YAML

  depends_on = [
    helm_release.cert-manager,
  ]
}


resource "kubectl_manifest" "cert-manager-cluster-issue" {
  yaml_body = <<YAML
apiVersion: cert-manager.io/v1
kind: ClusterIssuer
metadata:
  name: vault-issuer
spec:
  vault:
    server: http://vault.vault:8200
    path: pki/sign/cluster-dot-local
    auth:
      kubernetes:
        mountPath: /v1/auth/kubernetes
        role: issuer
        secretRef:
          name: issuer-token-lmzpj
          key: token
YAML

  depends_on = [
    helm_release.cert-manager,
  ]
}


resource "kubectl_manifest" "cert-manager-example" {
  yaml_body = <<YAML
apiVersion: cert-manager.io/v1
kind: Certificate
metadata:
  name: k8s-local
  namespace: cert-manager
spec:
  secretName: k8s-local-tls
  issuerRef:
    name: vault-issuer
  commonName: www.${var.dns_zone}
  dnsNames:
  - www.${var.dns_zone}
YAML

  depends_on = [
    helm_release.cert-manager,
  ]

}


resource "helm_release" "cert-manager" {
  name      = "cert-manager"
  namespace = kubernetes_namespace.cert-manager.metadata[0].name

  repository = "https://charts.jetstack.io"
  chart      = "cert-manager"
  version    = "1.13.3"

  values = [
    yamlencode({
      installCRDs = true
      "resources" = {
        "limits" = {
          "cpu"    = "0.5m"
          "memory" = "256Mi"
        }
        "requests" = {
          "cpu"    = "0.1m"
          "memory" = "50Mi"
        }
      }
  })]
}