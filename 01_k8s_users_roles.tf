resource "kubernetes_role" "developers" {
  for_each    = var.developer_users  
  metadata {
    namespace = each.value.namespace
    name      = each.value.name
  }

  rule {
    api_groups = [""]
    resources  = ["pods", "pods/log", "pods/exec", "pods/portforward", "events"]
    verbs      = ["get", "list", "delete", "watch", "create"]
  }

  rule {
    api_groups = ["*"]
    resources  = ["ingresses", "deployments", "jobs", "cronjobs", "replicasets", "statefulsets", "services"]
    verbs      = ["get", "list", "watch"]
  }

  depends_on = [ kubernetes_namespace.namespaces ]
}