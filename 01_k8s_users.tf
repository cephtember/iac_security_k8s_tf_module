resource "kubernetes_service_account" "developers" {
  for_each    = var.developer_users

  metadata {
    name      = each.value.name
    namespace = each.value.namespace
  }

  depends_on = [
    kubernetes_namespace.namespaces
  ]
}


resource "kubernetes_secret" "developers" {
  for_each    = var.developer_users
  metadata {
    name      = "${each.value.name}-secret"
    namespace = each.value.namespace
    annotations = {
      "kubernetes.io/service-account.name" = "${each.value.name}"
    }
  }

  depends_on = [
    kubernetes_namespace.namespaces
  ]

  type = "kubernetes.io/service-account-token"
}


resource "kubernetes_role_binding" "developers" {
  for_each    = var.developer_users
  metadata {
    name      = "${each.value.namespace}-binding-${each.value.name}"
    namespace = "${each.value.namespace}"
  }

  subject {
    kind      = "ServiceAccount"
    name      = "${each.value.name}"
    namespace = "${each.value.namespace}"
  }

  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "Role"
    name      = "${each.value.name}"
  }

  depends_on = [
    kubernetes_namespace.namespaces
  ]
}